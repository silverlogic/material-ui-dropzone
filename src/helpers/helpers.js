export function isImage(file) {
    const fileName = file.name || file.path;
    /*
      This fails in cases like this image-6.37.47.png
      const suffix = fileName.substr(fileName.indexOf('.') + 1).toLowerCase();
      if (suffix === 'jpg' || suffix === 'jpeg' || suffix === 'bmp' || suffix === 'png') {
          return true;
      }
    */
    return /\.((jpg)|(jpeg)|(bmp)|(png))$/g.test(fileName)
}
export function convertBytesToMbsOrKbs(filesize){
  var size = '';
  // I know, not technically correct...
  if(filesize >= 1000000){
    size = (filesize / 1000000) + ' megabytes';
  }else if(filesize >= 1000){
    size = (filesize / 1000) + ' kilobytes';
  }else{
    size = filesize + ' bytes' 
  }
  return size
}